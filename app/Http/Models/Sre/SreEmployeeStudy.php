<?php

namespace App\Http\Models\Sre;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Models\SreCats\SreCatEducationLevel;


class SreEmployeeStudy extends Model
{
    use SoftDeletes;

    protected $fillable = [
			'sre_employee_id',
			'sre_cat_education_level_id',
			'startDate',
			'endDateDate'
    ];

    function educationLevel()
    {
      return $this->belongsTo(SreCatEducationLevel::class, 'sre_cat_education_level_id');
    }


}
