<?php

namespace App\Http\Models\Sre;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Models\Sre\SreFile;

class SreEmployeeDocument extends Model
{
    use SoftDeletes;

    protected $fillable = [
							'sre_employee_id',
							'sre_file_id',
							'fileNameSiar',
							'sre_cat_document_status_id',
							'sre_cat_document_type_id'
              ];

    function file()
    {
        return $this->belongsTo(SreFile::class, 'sre_file_id');
    }

}
