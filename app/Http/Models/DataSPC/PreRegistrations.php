<?php

namespace App\Http\Models\DataSPC;

use Illuminate\Database\Eloquent\Model;
use App\Http\Models\Geo\GeoCatCountry;
use App\Http\Models\SreCats\SreCatOffice;

class PreRegistrations extends Model
{
  protected $guarded = [];

  function affected()
	{
		return $this->belongsTo(People::class, 'person_affected_id');
	}
  function appearing()
	{
		return $this->belongsTo(People::class, 'person_appearing_id');
	}
  function affectedContact()
  {
    return $this->belongsTo(Contacts::class, 'contact_affected_id');
  }
  function appearingContact()
	{
		return $this->belongsTo(Contacts::class, 'contact_appearing_id');
	}
  function office()
	{
		return $this->belongsTo(SreCatOffice::class, 'sre_cat_office_id');
	}

}
