<?php

namespace App\Http\Models\DataSPC;

use Illuminate\Database\Eloquent\Model;
use App\Http\Models\Geo\GeoCatCountry;
use App\Http\Models\Geo\GeoCatState;
use App\Http\Models\Geo\GeoCatMunicipalities;

class Contacts extends Model
{
    protected $guarded = [];
    function contactCountry()
    {
      return $this->belongsTo(GeoCatCountry::class, 'geo_cat_country_id');
    }
    function contactState()
    {
      return $this->belongsTo(GeoCatState::class, 'geo_cat_state_id');
    }
    function contactMunicipality()
    {
      return $this->belongsTo(GeoCatMunicipalities::class, 'geo_cat_municipality_id');
    }
}
