<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\DataSPC\People;
use App\Http\Models\DataSPC\Contacts;
use App\Http\Models\DataSPC\PreRegistrations;
use App\Http\Models\SreCats\SreCatOffice;
use Illuminate\Support\Facades\DB;

class PreRegistryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $filters = $request->all();
        //return $filters['dateRegister'];
        $filterNames = function($q) use($filters){
          $q->when(trim($filters['names']!=''), function ($query) use ($filters) {
                      return $query->where(DB::raw("CONCAT(name,' ',surname,' ',secondSurname)"), 'like', '%'.$filters['names'].'%');

                  });
        };

        $preRegistration = PreRegistrations::with([
        'affected'=>$filterNames,
        'affectedContact.contactCountry',
        'affectedContact.contactState',
        'affectedContact.contactMunicipality',
        'appearing',
        'appearingContact.contactCountry',
        'appearingContact.contactState',
        'appearingContact.contactMunicipality',
        'appearingContact',
        'office']
        )
        ->whereHas('affected',$filterNames)
        ->where(function ($q) use ($filters) {
          $q->when(trim($filters['folio']!=''), function ($query) use ($filters) {
                      return $query->where('folio','like','%'.$filters['folio'].'%');
                  });
          })
        ->where(function ($q) use ($filters) {
          $q->when(trim($filters['dateRegister']!=''), function ($query) use ($filters) {
            $from = $filters['dateRegister'][0]; //need a space after dates.
            $to = date('Y-m-d' . ' 23:59:59', strtotime(str_replace('-','/', $filters['dateRegister'][1])));
                      return $query->whereBetween('created_at',array($from, $to));
                  });
          })
          ->where(function ($q) use ($filters) {
            $q->when(trim($filters['representation']!=''), function ($query) use ($filters) {
                        return $query->where('sre_cat_office_id',$filters['representation']);
                    });
            })
          ->where(function ($q) use ($filters) {
            $q->when(trim($filters['status']!=''), function ($query) use ($filters) {
                        return $query->where('status',$filters['status']);
                    });
            })
        ->paginate(5);
        //->toSql();
        return [
          'pagination' =>[
            'total'         => $preRegistration->total(),
            'current_page'  => $preRegistration->currentPage(),
            'per_page'      => $preRegistration->perPage(),
            'last_page'     => $preRegistration->lastPage(),
            'from'          => $preRegistration->firstItem(),
            'to'            => $preRegistration->total(),
          ],
          'PreRegistrations' => $preRegistration
        ];


      //return response()->json($preRegistration);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $list = $request->lista;

        //Person affect
        $affected                 = new People();
        $affected->name           = $list['affectedName'];
        $affected->surname        = $list['affectedSurname'];
        $affected->secondSurname  = $list['affectedSecondSurname'];
        $affected->birthDate      = null;
        $affected->sex            = null;
        $affected->save();

        //Affect Contact
        $affectedContact                            = new Contacts();
        $affectedContact->geo_cat_country_id        = $list['countrySelect'];
        $affectedContact->geo_cat_state_id          = $list['stateSelect'];
        if($list['municipalitySelect'] > 0){
          $affectedContact->geo_cat_municipality_id = null;//$list['municipalitySelect'];
        }else {
          $affectedContact->geo_cat_municipality_id = null;
        }
        $affectedContact->phone                     = $list['affectedPhone'];
        $affectedContact->mail                      = $list['affectedMail'];
        $affectedContact->save();

        if($list['isAffected']=='false'){

          //Person appearing
          $appearing                 = new People();
          $appearing->name           = $list['appearingName'];
          $appearing->surname        = $list['appearingSurname'];
          $appearing->secondSurname  = $list['appearSecondSurname'];
          $appearing->birthDate      = null;
          $appearing->sex            = null;
          $appearing->save();

          //Affect Contact
          $appearingContact                            = new Contacts();
          $appearingContact->geo_cat_country_id        = $list['appearingCountrySelect'];
          $appearingContact->geo_cat_state_id          = $list['appearingStateSelect'];
          if($list['municipalitySelect'] > 0){
            $appearingContact->geo_cat_municipality_id = $list['appearingMunicipalitySelect'];
          }else {
            $appearingContact->geo_cat_municipality_id = null;
          }
          $appearingContact->phone                     = $list['appearingPhone'];
          $appearingContact->mail                      = $list['appearingMail'];
          $appearingContact->save();

        }

        //PreRegistration
        $preRegistration = new PreRegistrations();
        $preRegistration->person_affected_id      = $affected->id;
        $preRegistration->contact_affected_id     = $affectedContact->id;
        if($list['isAffected']=='false'){
          $preRegistration->person_appearing_id   = $appearing->id;
          $preRegistration->contact_appearing_id  = $appearingContact->id;

          $qState   = SreCatOffice::where('geo_cat_state_id',$list['appearingstateSelect']);
          $qmunicipality = SreCatOffice::where('geo_cat_municipality_id',$list['appearingmunicipalitySelect']);

          $office = SreCatOffice::where(function ($q) use ($list, $qState, $qmunicipality) {
                                $q->when($list['appearingCountrySelect'] > 0 && $list['appearingStateSelect'] > 0 && $list['appearingMunicipalitySelect'] > 0, function ($query) use ($list, $qState, $qmunicipality) {
                                            return $query->where('geo_cat_country_id',$list['appearingCountrySelect'])
                                                         ->union($qState)
                                                         ->union($qmunicipality);
                                        });
                                $q->when($list['appearingCountrySelect'] > 0 && $list['appearingStateSelect'] > 0 , function ($query) use ($list, $qState) {
                                            return $query->where('geo_cat_country_id',$list['appearingCountrySelect'])
                                                        ->union($qState);
                                        });
                                })
                              ->first();
          $preRegistration->sre_cat_office_id       = $office->id;
        }else {
          $preRegistration->person_appearing_id   = null;
          $preRegistration->contact_appearing_id  = null;

          $qState   = SreCatOffice::where('geo_cat_state_id',$list['stateSelect']);
          $qmunicipality = SreCatOffice::where('geo_cat_municipality_id',$list['municipalitySelect']);

          $office = SreCatOffice::where(function ($q) use ($list, $qState, $qmunicipality) {
                                $q->when($list['countrySelect'] > 0 && $list['stateSelect'] > 0 && $list['municipalitySelect'] > 0, function ($query) use ($list, $qState, $qmunicipality) {
                                            return $query->where('geo_cat_country_id',$list['countrySelect'])
                                                         ->union($qState)
                                                         ->union($qmunicipality);
                                        });
                                $q->when($list['countrySelect'] > 0 && $list['stateSelect'] > 0 , function ($query) use ($list, $qState) {
                                            return $query->where('geo_cat_country_id',$list['countrySelect'])
                                                        ->union($qState);
                                        });
                                })
                              ->first();

        $countPreRegister = PreRegistrations::where('sre_cat_office_id',$office->id)
                                              ->whereYear('created_at', '=', date('Y'))
                                              ->count()+1;
        $preRegistration->sre_cat_office_id       = $office->id;
        }
        $preRegistration->folio                   = $office->officeSiarIdentifier . '_' . date('Y') . '_' . $countPreRegister;
        $preRegistration->eventDescription        = $list['eventDescription'];
        $preRegistration->status                  = 1;
        $preRegistration->save();
        $folioOffice = [$preRegistration->folio, $office->longName];

        return $folioOffice;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $preRegistration = PreRegistrations::find($id);
        $preRegistration->status = 3;
        $preRegistration->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
