<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Models\Oracle\OracleSreEmployee;
use App\Http\Models\Sre\SreEmployee;

class SeedEmployeesFromOracle extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'seed:employees';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Employees information from Oracle Database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $employees = OracleSreEmployee::all();
        foreach ($employees as $employee) {
            $newEmployee = new SreEmployee;
            $newEmployee->employeeNumber               = $employee->no_empleado;
            $newEmployee->name                         = $employee->nombres;
            $newEmployee->firstName                    = $employee->apellido_pat;
            $newEmployee->secondName                   = $employee->apellido_mat;
            $newEmployee->sre_cat_employee_status_id   = $employee->estatus == 0 ? 2 : $employee->estatus;
            $newEmployee->workEmail                    = $employee->email_trabajo;
            $newEmployee->startDate                    = $employee->f_ing_sre;
            $newEmployee->streetName                   = $employee->calle;
            $newEmployee->buldingNum                   = $employee->no_exterior;
            $newEmployee->apartmentNum                 = $employee->no_interior;
            $newEmployee->neighborhood                 = $employee->colonia;
            $newEmployee->postalCode                   = $employee->codigo_postal;
            $newEmployee->telephoneNumber              = $employee->telefono;
            $newEmployee->mobileNumber                 = $employee->celular;
            $newEmployee->personalEmail                = $employee->email_personal;
            $newEmployee->rfc                          = $employee->rfc;
            $newEmployee->curp                         = $employee->curp;
            $newEmployee->identificationCode           = $employee->clave_elector;
            $newEmployee->identificationFolio          = $employee->folio_elector;
            $newEmployee->educationCode                = $employee->cedula_profesional;
            $newEmployee->driverCode                   = $employee->cartilla_smn;
            $newEmployee->armyCode                     = $employee->cartilla_smn;
            $newEmployee->nss                          = $employee->nss;
            $newEmployee->isste                        = $employee->no_issste;
            $newEmployee->regimenIsste                 = $employee->regimen_issste;
            $newEmployee->sre_cat_employee_gender_id   = $employee->genero == 0 ? null : $employee->genero;
            $newEmployee->save();
        }
        $this->info('Employees added succesfully to GMM database');
    }
}
