<?php

Route::group( ['middleware' => ['auth:api'] ], function () {

});

Route::resource('countries', 'CountriesController');
Route::resource('states', 'StatesController');
Route::resource('municipalities', 'MunicipalitiesController');
Route::resource('preregistrations', 'PreRegistryController');
Route::resource('offices', 'OfficeController');
