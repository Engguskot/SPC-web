require('./main');

import App from './components/views/layout/App.vue';
// Router and routes
import VueRouter from 'vue-router';
import { routes } from './routes.js';
// Shared data using Vuex
import store from './store/index.js'

const router = new VueRouter({
	mode: 'history',
	base: __dirname,
	linkActiveClass: 'active',
	routes: routes,
	scrollBehavior(to, from, savedPosition) {
		return { x: 0, y: 0 };
	}
});

new Vue({
	el: '#app',
	router,
	store,
	render: h => h(App)
});

// Action to fix sesion user information if window browser is refreshed.
// Looking for a better solution
if ( window.localStorage.getItem('spc_session') ) {
	if( store.state.user.fullname === null ) {
		axios.get('/user/' + window.localStorage.getItem('spc_user'))
		.then( response => {
			store.commit('setUser', response.data.user);
		});
	}
}