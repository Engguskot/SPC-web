// Initial state
const state = {
	fullname		: null,
	name			: null,
	firstName		: null,
	secondName		: null,
	profile			: null
};

// Getters
const getters = {

};

// Actions
const actions = {

};

// Mutations
const mutations = {
	setUser: (state, user) => {
		state.fullname		= user.fullname;
		state.name			= user.name;
		state.firstName		= user.firstName;
		state.secondName	= user.secondName;
		state.profile		= user.profile;
	},
	deleteUser: state => {
		state.fullname		= null;
		state.name			= null;
		state.firstName		= null;
		state.secondName	= null;
		state.profile		= null;
	}
};

export default {
	state,
	getters,
	actions,
	mutations
}