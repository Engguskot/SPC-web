import { RouterView } from './RouterView.js';
import Movements from '../components/views/movements/MovementsIndex';

export default {
		path: 'movimientos',
		component: RouterView,
		children: [
			{
				path: '',
				component: Movements
			}
		]
}