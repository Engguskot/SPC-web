import { RouterView } from './RouterView.js';
import statistics from '../components/views/statistics/statistics';

export default {
		path: 'estadisticas',
		component: RouterView,
		children: [
			{
				path: 'consulta',
				component: statistics
			}
		]
}