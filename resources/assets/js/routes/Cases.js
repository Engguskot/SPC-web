import { RouterView } from './RouterView.js';
import TitularRegister from '../components/views/cases/TitularRegister';
import CasesSearch from '../components/views/cases/CasesSearch';

export default {
		path: 'casos',
		component: RouterView,
		children: [
			{
				path: 'registro',
				component: TitularRegister
			},
			{
				path: 'consulta',
				component: CasesSearch
			}
		]
}