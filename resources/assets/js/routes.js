// login
import Login	from './components/views/login/Form';
// Panel
import Panel	from './components/views/panel/Home';
import DashBoard	from './components/views/panel/DashBoard';
import DashBoardPreRegistro	from './components/views/preRegistro/DashBoardPreRegistro';
import PreRegistro	from './components/views/preRegistro/PreRegistro';
import BandejaPreRegistro	from './components/views/preRegistro/BandejaPreRegistro';

import DynamicForm from './components/views/admin/dynamic_forms/DynamicForm';
import MockUp from './components/views/MockUp';
//Casos
import CasesRoutes from './routes/Cases';
//Cargas masivass
import BulkLoadRoutes from './routes/BulkLoad';
//Casos grupales
import GroupSearch from './routes/Groups';
//Estadisticas
import StatisticsRoutes from './routes/Statistics';

export const routes = [
	{
		path: '/ingresar',
		component: Login,
		beforeEnter(to, from, next) {
			if( localStorage.getItem('gmm_token')) {
				next('/');
			} else {
				next();
			}
		}
	},
	{
		path: '/',
		component: Panel,
		children: [
			{
				path: '',
				component: DashBoard
			},
			{
				path: '/BandejaPr',
				component: BandejaPreRegistro
			},
			{
				path: 'admin/forms',
				component: DynamicForm
			}
			,
			{
				path: 'mockup',
				component: MockUp
			},
			{...CasesRoutes},
			{...BulkLoadRoutes},
			{...GroupSearch},
			{...StatisticsRoutes}
		],
		beforeEnter(to, from, next) {
			if ( localStorage.getItem('spc_token') ) {
				next();
			} else {
				next('/ingresar');
			}
		},
	},
	{
		path: '/preRegistro',
		component: DashBoardPreRegistro,
		children: [
			{
				path: '',
				component: PreRegistro
			}

		]
	}
];
