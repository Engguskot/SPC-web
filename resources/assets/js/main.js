// Vue, Vuex, Router and Axios
import Vue from "vue"
import Vuex from 'vuex'
import VueRouter from "vue-router"
import axios from 'axios'
// Custom CSS
require('normalize.css')
require('../css/app.css')
require('../css/fonts.css')
// Element UI
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
require('../css/element-ui-theme/index.css')
import locale from 'element-ui/lib/locale/lang/es'
// Font Awesome Icons
require('../css/font-awesome/css/fontawesome-all.min.css')
// Moment JS with Spanish Translation
import moment from 'moment'
require( 'moment/locale/es' )

Vue.use( Vuex )
Vue.use( VueRouter )
Vue.use( ElementUI, { locale } )
Vue.use( require('vue-moment') )

window.Vue = Vue
window.axios = axios

axios.defaults.headers.common = {
<<<<<<< HEAD
	'Authorization': 'Bearer ' + window.localStorage.getItem('gmm_token')
};
=======
	'Authorization': 'Bearer ' + window.localStorage.getItem('spc_token')
};
>>>>>>> 76e9c96644e238dfb03031796d0359397223c68d
