<?php

use Illuminate\Database\Seeder;

class SreUsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('sre_users')->delete();
        
        \DB::table('sre_users')->insert(array (
            
            array (
                'id' => 1,
                'username' => 'jrojo',
                'sre_cat_profile_id' => 1,
                'password' => '$2y$10$pN9/BypbR3StR84o/qcw/uFF8aNPjyEUMRqcHsosGcULFXqv/rmEu',
                'isLdap' => 0
            ),
            
            array (
                'id' => 2,
                'username' => 'marcosp',
                'sre_cat_profile_id' => 1,
                'password' => '$2y$10$pN9/BypbR3StR84o/qcw/uFF8aNPjyEUMRqcHsosGcULFXqv/rmEu',
                'isLdap' => 0
            ),
            
            array (
                'id' => 3,
                'username' => 'ratecas',
                'sre_cat_profile_id' => 1,
                'password' => '$2y$10$pN9/BypbR3StR84o/qcw/uFF8aNPjyEUMRqcHsosGcULFXqv/rmEu',
                'isLdap' => 0
            ),
            
            array (
                'id' => 4,
                'username' => 'pcastillor',
                'sre_cat_profile_id' => 1,
                'password' => '$2y$10$pN9/BypbR3StR84o/qcw/uFF8aNPjyEUMRqcHsosGcULFXqv/rmEu',
                'isLdap' => 0
            ),
            
            array (
                'id' => 5,
                'username' => 'hpiedras',
                'sre_cat_profile_id' => 1,
                'password' => '$2y$10$pN9/BypbR3StR84o/qcw/uFF8aNPjyEUMRqcHsosGcULFXqv/rmEu',
                'isLdap' => 0
            ),
            
            array (
                'id' => 6,
                'username' => 'haguirre',
                'sre_cat_profile_id' => 1,
                'password' => '$2y$10$pN9/BypbR3StR84o/qcw/uFF8aNPjyEUMRqcHsosGcULFXqv/rmEu',
                'isLdap' => 0
            )

        ));
        
        
    }
}