<?php

use Illuminate\Database\Seeder;

class SreCatSubDepartmentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        
        \DB::table('sre_cat_sub_departments')->insert(array (
            
            array (
                'id' => 1,
                'name' => 'Dirección General Adjunta para el Ceremonial',
                'sre_cat_department_id' => 7,
                'created_at' => '2018-01-14 05:40:34',
                'updated_at' => '2018-01-14 05:40:34'
            ),
            
            array (
                'id' => 2,
                'name' => 'Dirección General Adjunta para Misiones Extranjeras',
                'sre_cat_department_id' => 7,
                'created_at' => '2018-01-14 05:40:34',
                'updated_at' => '2018-01-14 05:40:34'
            ),
            
            array (
                'id' => 3,
            'name' => 'Oficina de Protocolo en el Aeropuerto Internacional de la Ciudad de México (AICM)',
                'sre_cat_department_id' => 7,
                'created_at' => '2018-01-14 05:40:34',
                'updated_at' => '2018-01-14 05:40:34'
            ),
            
            array (
                'id' => 4,
                'name' => 'Dirección General Adjunta de Coordinación Política',
                'sre_cat_department_id' => 8,
                'created_at' => '2018-01-14 05:40:35',
                'updated_at' => '2018-01-14 05:40:35'
            ),
            
            array (
                'id' => 5,
                'name' => 'Dirección General Adjunta de Vinculación con el Congreso',
                'sre_cat_department_id' => 8,
                'created_at' => '2018-01-14 05:40:35',
                'updated_at' => '2018-01-14 05:40:35'
            ),
            
            array (
                'id' => 6,
                'name' => 'Dirección General Adjunta de Información',
                'sre_cat_department_id' => 9,
                'created_at' => '2018-01-14 05:40:35',
                'updated_at' => '2018-01-14 05:40:35'
            ),
            
            array (
                'id' => 7,
                'name' => 'Dirección General Adjunta de Información Nacional',
                'sre_cat_department_id' => 9,
                'created_at' => '2018-01-14 05:40:35',
                'updated_at' => '2018-01-14 05:40:35'
            ),
            
            array (
                'id' => 8,
                'name' => 'Dirección General Adjunta de Imagen Pública y Campañas',
                'sre_cat_department_id' => 9,
                'created_at' => '2018-01-14 05:40:35',
                'updated_at' => '2018-01-14 05:40:35'
            ),
            
            array (
                'id' => 9,
                'name' => 'Dirección General Adjunta de Análisis Estratégico',
                'sre_cat_department_id' => 9,
                'created_at' => '2018-01-14 05:40:35',
                'updated_at' => '2018-01-14 05:40:35'
            ),
            
            array (
                'id' => 10,
                'name' => 'Dirección General Adjunta',
                'sre_cat_department_id' => 10,
                'created_at' => '2018-01-14 05:40:35',
                'updated_at' => '2018-01-14 05:40:35'
            ),
            
            array (
                'id' => 11,
                'name' => 'Dirección de Nacionalidad y Naturalización',
                'sre_cat_department_id' => 10,
                'created_at' => '2018-01-14 05:40:35',
                'updated_at' => '2018-01-14 05:40:35'
            ),
            
            array (
                'id' => 12,
                'name' => 'Dirección de Permisos Artículo 27 Constitucional',
                'sre_cat_department_id' => 10,
                'created_at' => '2018-01-14 05:40:35',
                'updated_at' => '2018-01-14 05:40:35'
            ),
            
            array (
                'id' => 13,
                'name' => 'Dirección Jurídico Contencioso',
                'sre_cat_department_id' => 10,
                'created_at' => '2018-01-14 05:40:35',
                'updated_at' => '2018-01-14 05:40:35'
            ),
            
            array (
                'id' => 14,
                'name' => 'Dirección de los Contencioso Administrativo',
                'sre_cat_department_id' => 10,
                'created_at' => '2018-01-14 05:40:35',
                'updated_at' => '2018-01-14 05:40:35'
            ),
            
            array (
                'id' => 15,
                'name' => 'Dirección Consultiva y de Normatividad',
                'sre_cat_department_id' => 10,
                'created_at' => '2018-01-14 05:40:35',
                'updated_at' => '2018-01-14 05:40:35'
            ),
            
            array (
                'id' => 16,
                'name' => 'Dirección General Adjunta de Vinculación',
                'sre_cat_department_id' => 11,
                'created_at' => '2018-01-14 05:40:35',
                'updated_at' => '2018-01-14 05:40:35'
            ),
            
            array (
                'id' => 17,
                'name' => 'Dirección General Adjunta de la Adacemía Diplomática',
                'sre_cat_department_id' => 11,
                'created_at' => '2018-01-14 05:40:35',
                'updated_at' => '2018-01-14 05:40:35'
            ),
            
            array (
                'id' => 18,
                'name' => 'Dirección General Adjunta para Asia-Pacífico',
                'sre_cat_department_id' => 12,
                'created_at' => '2018-01-14 05:40:35',
                'updated_at' => '2018-01-14 05:40:35'
            ),
            
            array (
                'id' => 19,
                'name' => 'Dirección General Adjunta para la Península Coreana, India, El Pacífico y Mecanismos de Cooperación Transpacifica',
                'sre_cat_department_id' => 12,
                'created_at' => '2018-01-14 05:40:35',
                'updated_at' => '2018-01-14 05:40:35'
            ),
            
            array (
                'id' => 20,
                'name' => 'Dirección General Adjunta Para África',
                'sre_cat_department_id' => 13,
                'created_at' => '2018-01-14 05:40:35',
                'updated_at' => '2018-01-14 05:40:35'
            ),
            
            array (
                'id' => 21,
                'name' => 'Dirección para África',
                'sre_cat_department_id' => 13,
                'created_at' => '2018-01-14 05:40:35',
                'updated_at' => '2018-01-14 05:40:35'
            ),
            
            array (
                'id' => 22,
                'name' => 'Dirección General Adjunta para Medio Oriente y Asia Central',
                'sre_cat_department_id' => 13,
                'created_at' => '2018-01-14 05:40:35',
                'updated_at' => '2018-01-14 05:40:35'
            ),
            
            array (
                'id' => 23,
                'name' => 'Dirección para Medio Oriente',
                'sre_cat_department_id' => 13,
                'created_at' => '2018-01-14 05:40:35',
                'updated_at' => '2018-01-14 05:40:35'
            ),
            
            array (
                'id' => 24,
                'name' => 'Dirección General Adjunta para la Uni+on Europera, el Consejo de Europa, Benelux, Irlanda y Reino Unido',
                'sre_cat_department_id' => 14,
                'created_at' => '2018-01-14 05:40:35',
                'updated_at' => '2018-01-14 05:40:35'
            ),
            
            array (
                'id' => 25,
                'name' => 'Dirección General Adjunta para Países Mediterráneos y Nórdicos',
                'sre_cat_department_id' => 14,
                'created_at' => '2018-01-14 05:40:36',
                'updated_at' => '2018-01-14 05:40:36'
            ),
            
            array (
                'id' => 26,
                'name' => 'Dirección General Adjunta para Países de Europa Central, del Este, Balcánicos y del Cáucaso',
                'sre_cat_department_id' => 14,
                'created_at' => '2018-01-14 05:40:36',
                'updated_at' => '2018-01-14 05:40:36'
            ),
            
            array (
                'id' => 27,
                'name' => 'Dirección General Adjunta de Competitividad e Innovación con América del Norte',
                'sre_cat_department_id' => 15,
                'created_at' => '2018-01-14 05:40:36',
                'updated_at' => '2018-01-14 05:40:36'
            ),
            
            array (
                'id' => 28,
                'name' => 'Dirección General Adjunta para Asuntos Políticos para América del Norte',
                'sre_cat_department_id' => 15,
                'created_at' => '2018-01-14 05:40:36',
                'updated_at' => '2018-01-14 05:40:36'
            ),
            
            array (
                'id' => 29,
                'name' => 'Dirección General Adjunta para Asuntos Fronterizos',
                'sre_cat_department_id' => 15,
                'created_at' => '2018-01-14 05:40:36',
                'updated_at' => '2018-01-14 05:40:36'
            ),
            
            array (
                'id' => 30,
                'name' => 'Dirección de Procesos de Protección y Programas Institucionales',
                'sre_cat_department_id' => 16,
                'created_at' => '2018-01-14 05:40:36',
                'updated_at' => '2018-01-14 05:40:36'
            ),
            
            array (
                'id' => 31,
                'name' => 'Dirección General Adjunta de Protección',
                'sre_cat_department_id' => 16,
                'created_at' => '2018-01-14 05:40:36',
                'updated_at' => '2018-01-14 05:40:36'
            ),
            
            array (
                'id' => 32,
                'name' => 'Dirección de Protección para Estados Unidos de América',
                'sre_cat_department_id' => 16,
                'created_at' => '2018-01-14 05:40:36',
                'updated_at' => '2018-01-14 05:40:36'
            ),
            
            array (
                'id' => 33,
                'name' => 'Dirección de Protección en el Resto del Mundo',
                'sre_cat_department_id' => 16,
                'created_at' => '2018-01-14 05:40:36',
                'updated_at' => '2018-01-14 05:40:36'
            ),
            
            array (
                'id' => 34,
                'name' => 'Dirección General Adjunta de Derecho de Familia',
                'sre_cat_department_id' => 16,
                'created_at' => '2018-01-14 05:40:36',
                'updated_at' => '2018-01-14 05:40:36'
            ),
            
            array (
                'id' => 35,
                'name' => 'Dirección General Adjunta de Políticas de Protección',
                'sre_cat_department_id' => 16,
                'created_at' => '2018-01-14 05:40:36',
                'updated_at' => '2018-01-14 05:40:36'
            ),
            
            array (
                'id' => 36,
                'name' => 'Dirección de Información de Políticas de Protección',
                'sre_cat_department_id' => 16,
                'created_at' => '2018-01-14 05:40:36',
                'updated_at' => '2018-01-14 05:40:36'
            ),
            
            array (
                'id' => 37,
                'name' => 'Dirección de Análisis y Prospectiva',
                'sre_cat_department_id' => 16,
                'created_at' => '2018-01-14 05:40:36',
                'updated_at' => '2018-01-14 05:40:36'
            ),
            
            array (
                'id' => 38,
                'name' => 'Dirección General Adjunta de Modernización y Administración Consular',
                'sre_cat_department_id' => 17,
                'created_at' => '2018-01-14 05:40:36',
                'updated_at' => '2018-01-14 05:40:36'
            ),
            
            array (
                'id' => 39,
                'name' => 'Dirección de Recursos Consulares',
                'sre_cat_department_id' => 17,
                'created_at' => '2018-01-14 05:40:36',
                'updated_at' => '2018-01-14 05:40:36'
            ),
            
            array (
                'id' => 40,
                'name' => 'Dirección de Prosectiva e Innovación Consular',
                'sre_cat_department_id' => 17,
                'created_at' => '2018-01-14 05:40:36',
                'updated_at' => '2018-01-14 05:40:36'
            ),
            
            array (
                'id' => 41,
                'name' => 'Dirección de Proyectos Especiales',
                'sre_cat_department_id' => 17,
                'created_at' => '2018-01-14 05:40:36',
                'updated_at' => '2018-01-14 05:40:36'
            ),
            
            array (
                'id' => 42,
                'name' => 'Dirección General Adjunta de Servicios Consulares',
                'sre_cat_department_id' => 17,
                'created_at' => '2018-01-14 05:40:36',
                'updated_at' => '2018-01-14 05:40:36'
            ),
            
            array (
                'id' => 43,
                'name' => 'Dirección de Servicios Consulares',
                'sre_cat_department_id' => 17,
                'created_at' => '2018-01-14 05:40:36',
                'updated_at' => '2018-01-14 05:40:36'
            ),
            
            array (
                'id' => 44,
                'name' => 'Dirección General Adjunta de Servicios Migratorios',
                'sre_cat_department_id' => 17,
                'created_at' => '2018-01-14 05:40:36',
                'updated_at' => '2018-01-14 05:40:36'
            ),
            
            array (
                'id' => 45,
                'name' => 'Dirección de Normatividad Migratoria',
                'sre_cat_department_id' => 17,
                'created_at' => '2018-01-14 05:40:36',
                'updated_at' => '2018-01-14 05:40:36'
            ),
            
            array (
                'id' => 46,
                'name' => 'Dirección de Operación Migratori',
                'sre_cat_department_id' => 17,
                'created_at' => '2018-01-14 05:40:36',
                'updated_at' => '2018-01-14 05:40:36'
            ),
            
            array (
                'id' => 47,
            'name' => 'Oficina de Enlace en el Instituto Nacional de Migración (INM)',
                'sre_cat_department_id' => 17,
                'created_at' => '2018-01-14 05:40:36',
                'updated_at' => '2018-01-14 05:40:36'
            ),
            
            array (
                'id' => 48,
                'name' => 'Dirección General Adjunta de Cooperación Internacional Sobre Seguridad',
                'sre_cat_department_id' => 20,
                'created_at' => '2018-01-14 05:40:36',
                'updated_at' => '2018-01-14 05:40:36'
            ),
            
            array (
                'id' => 49,
                'name' => 'Dirección General Adjunta de Asuntos Especiales',
                'sre_cat_department_id' => 20,
                'created_at' => '2018-01-14 05:40:36',
                'updated_at' => '2018-01-14 05:40:36'
            ),
            
            array (
                'id' => 50,
                'name' => 'Dirección General Adjunta de Coordinación de Gestión y Análisis',
                'sre_cat_department_id' => 21,
                'created_at' => '2018-01-14 05:40:36',
                'updated_at' => '2018-01-14 05:40:36'
            ),
            
            array (
                'id' => 51,
                'name' => 'Dirección General Adjunta para Centroamérica, Cuba y República Dominicana',
                'sre_cat_department_id' => 21,
                'created_at' => '2018-01-14 05:40:36',
                'updated_at' => '2018-01-14 05:40:36'
            ),
            
            array (
                'id' => 52,
                'name' => 'Dirección General Adjunta para Frontera Sur y Migración Regional',
                'sre_cat_department_id' => 21,
                'created_at' => '2018-01-14 05:40:36',
                'updated_at' => '2018-01-14 05:40:36'
            ),
            
            array (
                'id' => 53,
                'name' => 'Dirección General Adjunta para el Caribe y Haití',
                'sre_cat_department_id' => 21,
                'created_at' => '2018-01-14 05:40:37',
                'updated_at' => '2018-01-14 05:40:37'
            ),
            
            array (
                'id' => 54,
                'name' => 'Dirección General Adjunta para América del Sur',
                'sre_cat_department_id' => 21,
                'created_at' => '2018-01-14 05:40:37',
                'updated_at' => '2018-01-14 05:40:37'
            ),
            
            array (
                'id' => 55,
                'name' => 'Dirección General Adjunta de Mecanismos Regionales',
                'sre_cat_department_id' => 22,
                'created_at' => '2018-01-14 05:40:37',
                'updated_at' => '2018-01-14 05:40:37'
            ),
            
            array (
                'id' => 56,
            'name' => 'Dirección para la Comunidad de Estados Latinoamericanos y Caribeños (CELEC) y Cumbre CELAC-Unión Europea (CELAC-UE)',
                'sre_cat_department_id' => 22,
                'created_at' => '2018-01-14 05:40:37',
                'updated_at' => '2018-01-14 05:40:37'
            ),
            
            array (
                'id' => 57,
                'name' => 'Dirección para la Confederación Iberoamericana',
                'sre_cat_department_id' => 22,
                'created_at' => '2018-01-14 05:40:37',
                'updated_at' => '2018-01-14 05:40:37'
            ),
            
            array (
                'id' => 58,
                'name' => 'Dirección General Adjunta para la Integración Regional en América Latina y el Caribe',
                'sre_cat_department_id' => 22,
                'created_at' => '2018-01-14 05:40:37',
                'updated_at' => '2018-01-14 05:40:37'
            ),
            
            array (
                'id' => 59,
                'name' => 'Dirección para Alianza del Pacífico',
                'sre_cat_department_id' => 22,
                'created_at' => '2018-01-14 05:40:37',
                'updated_at' => '2018-01-14 05:40:37'
            ),
            
            array (
                'id' => 60,
                'name' => 'Dirección de Organismos de Integración Económica',
                'sre_cat_department_id' => 22,
                'created_at' => '2018-01-14 05:40:37',
                'updated_at' => '2018-01-14 05:40:37'
            ),
            
            array (
                'id' => 61,
                'name' => 'Dirección General Adjunta para Asuntos Hemisféricos y de Seguridad',
                'sre_cat_department_id' => 22,
                'created_at' => '2018-01-14 05:40:37',
                'updated_at' => '2018-01-14 05:40:37'
            ),
            
            array (
                'id' => 62,
                'name' => 'Dirección para Asuntos Políticos y de Seguridad',
                'sre_cat_department_id' => 22,
                'created_at' => '2018-01-14 05:40:37',
                'updated_at' => '2018-01-14 05:40:37'
            ),
            
            array (
                'id' => 63,
                'name' => 'Dirección para Asuntos Migratorios, Sociales y Candidaturas',
                'sre_cat_department_id' => 22,
                'created_at' => '2018-01-14 05:40:37',
                'updated_at' => '2018-01-14 05:40:37'
            ),
            
            array (
                'id' => 64,
                'name' => 'Dirección para Asuntos Hemisféricos',
                'sre_cat_department_id' => 22,
                'created_at' => '2018-01-14 05:40:37',
                'updated_at' => '2018-01-14 05:40:37'
            ),
            
            array (
                'id' => 65,
                'name' => 'Control de Gestión y Archivo',
                'sre_cat_department_id' => 22,
                'created_at' => '2018-01-14 05:40:37',
                'updated_at' => '2018-01-14 05:40:37'
            ),
            
            array (
                'id' => 66,
                'name' => 'Dirección General Adjunta para Temas Globales',
                'sre_cat_department_id' => 24,
                'created_at' => '2018-01-14 05:40:37',
                'updated_at' => '2018-01-14 05:40:37'
            ),
            
            array (
                'id' => 67,
                'name' => 'Dirección de Cambio Climático',
                'sre_cat_department_id' => 24,
                'created_at' => '2018-01-14 05:40:37',
                'updated_at' => '2018-01-14 05:40:37'
            ),
            
            array (
                'id' => 68,
                'name' => 'Dirección General Adjunta para Asuntos Económicos y Sociales',
                'sre_cat_department_id' => 24,
                'created_at' => '2018-01-14 05:40:37',
                'updated_at' => '2018-01-14 05:40:37'
            ),
            
            array (
                'id' => 69,
                'name' => 'Dirección General Adjunta para Desarrolo Sostenible',
                'sre_cat_department_id' => 24,
                'created_at' => '2018-01-14 05:40:37',
                'updated_at' => '2018-01-14 05:40:37'
            ),
            
            array (
                'id' => 70,
                'name' => 'Dirección General Adjunta de Casos Democracia y Derechos Humanos',
                'sre_cat_department_id' => 25,
                'created_at' => '2018-01-14 05:40:37',
                'updated_at' => '2018-01-14 05:40:37'
            ),
            
            array (
                'id' => 71,
                'name' => 'Dirección de Casos',
                'sre_cat_department_id' => 25,
                'created_at' => '2018-01-14 05:40:37',
                'updated_at' => '2018-01-14 05:40:37'
            ),
            
            array (
                'id' => 72,
                'name' => 'Dirección de Migración Internacional y Refugio',
                'sre_cat_department_id' => 25,
                'created_at' => '2018-01-14 05:40:37',
                'updated_at' => '2018-01-14 05:40:37'
            ),
            
            array (
                'id' => 73,
                'name' => 'Dirección para Cooperación y Difusión',
                'sre_cat_department_id' => 25,
                'created_at' => '2018-01-14 05:40:37',
                'updated_at' => '2018-01-14 05:40:37'
            ),
            
            array (
                'id' => 74,
                'name' => 'Dirección General Adjunta de Política Internacional sobre Derechos Humanos I',
                'sre_cat_department_id' => 25,
                'created_at' => '2018-01-14 05:40:37',
                'updated_at' => '2018-01-14 05:40:37'
            ),
            
            array (
                'id' => 75,
                'name' => 'Dirección de Política Internacional de Derechos Civiles, Políticos y Democracia',
                'sre_cat_department_id' => 25,
                'created_at' => '2018-01-14 05:40:37',
                'updated_at' => '2018-01-14 05:40:37'
            ),
            
            array (
                'id' => 76,
                'name' => 'Dirección de Asuntos Internacionales de la Mujer',
                'sre_cat_department_id' => 25,
                'created_at' => '2018-01-14 05:40:37',
                'updated_at' => '2018-01-14 05:40:37'
            ),
            
            array (
                'id' => 77,
                'name' => 'Dirección General Adjunta de Política Internacional sobre Derechos Humanos II',
                'sre_cat_department_id' => 25,
                'created_at' => '2018-01-14 05:40:37',
                'updated_at' => '2018-01-14 05:40:37'
            ),
            
            array (
                'id' => 78,
                'name' => 'Dirección de Política Internacional de Derechos Económicos, Sociales y Culturales',
                'sre_cat_department_id' => 25,
                'created_at' => '2018-01-14 05:40:38',
                'updated_at' => '2018-01-14 05:40:38'
            ),
            
            array (
                'id' => 79,
                'name' => 'Dirección para la Eliminación de la Discriminación',
                'sre_cat_department_id' => 25,
                'created_at' => '2018-01-14 05:40:38',
                'updated_at' => '2018-01-14 05:40:38'
            ),
            
            array (
                'id' => 80,
                'name' => 'Dirección General Adjunta para Desarme y No Proliferación',
                'sre_cat_department_id' => 26,
                'created_at' => '2018-01-14 05:40:38',
                'updated_at' => '2018-01-14 05:40:38'
            ),
            
            array (
                'id' => 81,
                'name' => 'Dirección de Desarme Nuclear',
                'sre_cat_department_id' => 26,
                'created_at' => '2018-01-14 05:40:38',
                'updated_at' => '2018-01-14 05:40:38'
            ),
            
            array (
                'id' => 82,
                'name' => 'Dirección para Organismos, Foros y Mecanismos Establecidos por Tratados',
                'sre_cat_department_id' => 26,
                'created_at' => '2018-01-14 05:40:38',
                'updated_at' => '2018-01-14 05:40:38'
            ),
            
            array (
                'id' => 83,
                'name' => 'Dirección General Adjunta para Asuntos Políticos',
                'sre_cat_department_id' => 26,
                'created_at' => '2018-01-14 05:40:38',
                'updated_at' => '2018-01-14 05:40:38'
            ),
            
            array (
                'id' => 84,
                'name' => 'Dirección para el Mantenimiento de la Paz',
                'sre_cat_department_id' => 26,
                'created_at' => '2018-01-14 05:40:38',
                'updated_at' => '2018-01-14 05:40:38'
            ),
            
            array (
                'id' => 85,
                'name' => 'Dirección para Asuntos Políticos',
                'sre_cat_department_id' => 26,
                'created_at' => '2018-01-14 05:40:38',
                'updated_at' => '2018-01-14 05:40:38'
            ),
            
            array (
                'id' => 86,
                'name' => 'Dirección para Desarme Convencional y Controles de Exportación',
                'sre_cat_department_id' => 26,
                'created_at' => '2018-01-14 05:40:38',
                'updated_at' => '2018-01-14 05:40:38'
            ),
            
            array (
                'id' => 87,
                'name' => 'Dirección para Organismos Especializados I',
                'sre_cat_department_id' => 26,
                'created_at' => '2018-01-14 05:40:38',
                'updated_at' => '2018-01-14 05:40:38'
            ),
            
            array (
                'id' => 88,
                'name' => 'Dirección General Adjunta para Organismos Especializdos y Candidaturas',
                'sre_cat_department_id' => 26,
                'created_at' => '2018-01-14 05:40:38',
                'updated_at' => '2018-01-14 05:40:38'
            ),
            
            array (
                'id' => 89,
                'name' => 'Dirección de Planeación y Asuntos Financieros',
                'sre_cat_department_id' => 26,
                'created_at' => '2018-01-14 05:40:38',
                'updated_at' => '2018-01-14 05:40:38'
            ),
            
            array (
                'id' => 90,
                'name' => 'Dirección de Candidaturas',
                'sre_cat_department_id' => 26,
                'created_at' => '2018-01-14 05:40:38',
                'updated_at' => '2018-01-14 05:40:38'
            ),
            
            array (
                'id' => 91,
                'name' => 'Dirección General Adjunta para Seguridad Multidimensional',
                'sre_cat_department_id' => 26,
                'created_at' => '2018-01-14 05:40:38',
                'updated_at' => '2018-01-14 05:40:38'
            ),
            
            array (
                'id' => 92,
                'name' => 'Dirección para Prevencion del Delito y Combate a la Corrupción',
                'sre_cat_department_id' => 26,
                'created_at' => '2018-01-14 05:40:38',
                'updated_at' => '2018-01-14 05:40:38'
            ),
            
            array (
                'id' => 93,
                'name' => 'Dirección para Temas de Drogas y Ciberseguridad',
                'sre_cat_department_id' => 26,
                'created_at' => '2018-01-14 05:40:38',
                'updated_at' => '2018-01-14 05:40:38'
            ),
            
            array (
                'id' => 94,
                'name' => 'Dirección General Adjunta de Vinculación',
                'sre_cat_department_id' => 27,
                'created_at' => '2018-01-14 05:40:38',
                'updated_at' => '2018-01-14 05:40:38'
            ),
            
            array (
                'id' => 95,
                'name' => 'Dirección General Adjunta de Desarrollo',
                'sre_cat_department_id' => 27,
                'created_at' => '2018-01-14 05:40:38',
                'updated_at' => '2018-01-14 05:40:38'
            ),
            
            array (
                'id' => 96,
                'name' => 'Dirección de Relaciones Laborales',
                'sre_cat_department_id' => 28,
                'created_at' => '2018-01-14 05:40:38',
                'updated_at' => '2018-01-14 05:40:38'
            ),
            
            array (
                'id' => 97,
                'name' => 'Dirección para la Igualdad de Género',
                'sre_cat_department_id' => 28,
                'created_at' => '2018-01-14 05:40:38',
                'updated_at' => '2018-01-14 05:40:38'
            ),
            
            array (
                'id' => 98,
                'name' => 'Dirección Jurídica',
                'sre_cat_department_id' => 28,
                'created_at' => '2018-01-14 05:40:38',
                'updated_at' => '2018-01-14 05:40:38'
            ),
            
            array (
                'id' => 99,
                'name' => 'Dirección General Adjunta de Nómina y Presupuesto',
                'sre_cat_department_id' => 28,
                'created_at' => '2018-01-14 05:40:38',
                'updated_at' => '2018-01-14 05:40:38'
            ),
            
            array (
                'id' => 100,
                'name' => 'Dirección General Adjunta del Servicioi Exterior Mexicano',
                'sre_cat_department_id' => 28,
                'created_at' => '2018-01-14 05:40:38',
                'updated_at' => '2018-01-14 05:40:38'
            ),
            
            array (
                'id' => 101,
                'name' => 'Dirección del Servicio Exterior Mexicano',
                'sre_cat_department_id' => 28,
                'created_at' => '2018-01-14 05:40:38',
                'updated_at' => '2018-01-14 05:40:38'
            ),
            
            array (
                'id' => 102,
                'name' => 'Dirección Técnica del Servicio Exterior Mexicano',
                'sre_cat_department_id' => 28,
                'created_at' => '2018-01-14 05:40:38',
                'updated_at' => '2018-01-14 05:40:38'
            ),
            
            array (
                'id' => 103,
                'name' => 'Dirección de Empleados Locales',
                'sre_cat_department_id' => 28,
                'created_at' => '2018-01-14 05:40:39',
                'updated_at' => '2018-01-14 05:40:39'
            ),
            
            array (
                'id' => 104,
                'name' => 'Dirección General Adjunta de Recursos Humanos',
                'sre_cat_department_id' => 28,
                'created_at' => '2018-01-14 05:40:39',
                'updated_at' => '2018-01-14 05:40:39'
            ),
            
            array (
                'id' => 105,
                'name' => 'Dirección de Servicios al Personal',
                'sre_cat_department_id' => 28,
                'created_at' => '2018-01-14 05:40:39',
                'updated_at' => '2018-01-14 05:40:39'
            ),
            
            array (
                'id' => 106,
                'name' => 'Dirección de Innovación y Desarrolo de Capital Humano',
                'sre_cat_department_id' => 28,
                'created_at' => '2018-01-14 05:40:39',
                'updated_at' => '2018-01-14 05:40:39'
            ),
            
            array (
                'id' => 107,
                'name' => 'Dirección General Adjunta de Operación Presupuestal y Contabilidad',
                'sre_cat_department_id' => 29,
                'created_at' => '2018-01-14 05:40:39',
                'updated_at' => '2018-01-14 05:40:39'
            ),
            
            array (
                'id' => 108,
                'name' => 'Dirección de Programación y Presupuesto',
                'sre_cat_department_id' => 29,
                'created_at' => '2018-01-14 05:40:39',
                'updated_at' => '2018-01-14 05:40:39'
            ),
            
            array (
                'id' => 109,
                'name' => 'Dirección General Adjunta de Organización, Mejora Regulatoria y Seguimiento a Programas Institucionales',
                'sre_cat_department_id' => 29,
                'created_at' => '2018-01-14 05:40:39',
                'updated_at' => '2018-01-14 05:40:39'
            ),
            
            array (
                'id' => 110,
                'name' => 'Dirección General Adjunta de Finanzas y Tesorería',
                'sre_cat_department_id' => 29,
                'created_at' => '2018-01-14 05:40:39',
                'updated_at' => '2018-01-14 05:40:39'
            ),
            
            array (
                'id' => 111,
                'name' => 'Dirección General Adjunta de Arquitectura y Diseño',
                'sre_cat_department_id' => 30,
                'created_at' => '2018-01-14 05:40:39',
                'updated_at' => '2018-01-14 05:40:39'
            ),
            
            array (
                'id' => 112,
                'name' => 'Dirección General Adjunta de Infraestructura y Seguridad Tecnológica',
                'sre_cat_department_id' => 30,
                'created_at' => '2018-01-14 05:40:39',
                'updated_at' => '2018-01-14 05:40:39'
            ),
            
            array (
                'id' => 113,
                'name' => 'Dirección General Adjunta de Operación',
                'sre_cat_department_id' => 30,
                'created_at' => '2018-01-14 05:40:39',
                'updated_at' => '2018-01-14 05:40:39'
            ),
            
            array (
                'id' => 114,
                'name' => 'Dirección General Adjunta de Administración',
                'sre_cat_department_id' => 30,
                'created_at' => '2018-01-14 05:40:39',
                'updated_at' => '2018-01-14 05:40:39'
            ),
            
            array (
                'id' => 115,
                'name' => 'Dirección de Cumplimiento y Mejores Prácticas',
                'sre_cat_department_id' => 30,
                'created_at' => '2018-01-14 05:40:39',
                'updated_at' => '2018-01-14 05:40:39'
            ),
            
            array (
                'id' => 116,
                'name' => 'Dirección de Enlace de Recursos y Servicios',
                'sre_cat_department_id' => 31,
                'created_at' => '2018-01-14 05:40:39',
                'updated_at' => '2018-01-14 05:40:39'
            ),
            
            array (
                'id' => 117,
                'name' => 'Dirección General Adjunta de Delegaciones',
                'sre_cat_department_id' => 31,
                'created_at' => '2018-01-14 05:40:39',
                'updated_at' => '2018-01-14 05:40:39'
            ),
            
            array (
                'id' => 118,
                'name' => 'Dirección de Operación de Pasaportes',
                'sre_cat_department_id' => 31,
                'created_at' => '2018-01-14 05:40:39',
                'updated_at' => '2018-01-14 05:40:39'
            ),
            
            array (
                'id' => 119,
                'name' => 'Dirección de Equipamiento e Infraestructura',
                'sre_cat_department_id' => 31,
                'created_at' => '2018-01-14 05:40:39',
                'updated_at' => '2018-01-14 05:40:39'
            ),
            
            array (
                'id' => 120,
                'name' => 'Dirección General de Normatividad',
                'sre_cat_department_id' => 31,
                'created_at' => '2018-01-14 05:40:39',
                'updated_at' => '2018-01-14 05:40:39'
            ),
            
            array (
                'id' => 121,
                'name' => 'Dirección de Vinculación y Promoción',
                'sre_cat_department_id' => 31,
                'created_at' => '2018-01-14 05:40:39',
                'updated_at' => '2018-01-14 05:40:39'
            ),
            
            array (
                'id' => 122,
                'name' => 'Dirección General Adjunta de Planeación',
                'sre_cat_department_id' => 31,
                'created_at' => '2018-01-14 05:40:39',
                'updated_at' => '2018-01-14 05:40:39'
            ),
            
            array (
                'id' => 123,
                'name' => 'Dirección de Proyectos',
                'sre_cat_department_id' => 31,
                'created_at' => '2018-01-14 05:40:39',
                'updated_at' => '2018-01-14 05:40:39'
            ),
            
            array (
                'id' => 124,
                'name' => 'Dirección de Supervición',
                'sre_cat_department_id' => 31,
                'created_at' => '2018-01-14 05:40:39',
                'updated_at' => '2018-01-14 05:40:39'
            ),
            
            array (
                'id' => 125,
                'name' => 'Dirección General Adjunta de Bienes Inmuebles y Administración en el Exterior',
                'sre_cat_department_id' => 32,
                'created_at' => '2018-01-14 05:40:39',
                'updated_at' => '2018-01-14 05:40:39'
            ),
            
            array (
                'id' => 126,
                'name' => 'Dirección de Adquisiciones y Servicios en el Exterior',
                'sre_cat_department_id' => 32,
                'created_at' => '2018-01-14 05:40:39',
                'updated_at' => '2018-01-14 05:40:39'
            ),
            
            array (
                'id' => 127,
                'name' => 'Dirección de Bienes Inmuebles en el Exterior',
                'sre_cat_department_id' => 32,
                'created_at' => '2018-01-14 05:40:39',
                'updated_at' => '2018-01-14 05:40:39'
            ),
            
            array (
                'id' => 128,
                'name' => 'Dirección de Mantenimiento y Obras en el Exterior',
                'sre_cat_department_id' => 32,
                'created_at' => '2018-01-14 05:40:39',
                'updated_at' => '2018-01-14 05:40:39'
            ),
            
            array (
                'id' => 129,
                'name' => 'Dirección General Adjunta de Aquisiciones y Control de Bienes ',
                'sre_cat_department_id' => 32,
                'created_at' => '2018-01-14 05:40:39',
                'updated_at' => '2018-01-14 05:40:39'
            ),
            
            array (
                'id' => 130,
                'name' => 'Dirección de Control de Bienes',
                'sre_cat_department_id' => 32,
                'created_at' => '2018-01-14 05:40:40',
                'updated_at' => '2018-01-14 05:40:40'
            ),
            
            array (
                'id' => 131,
                'name' => 'Dirección de Adquisiciones y Contrataciones',
                'sre_cat_department_id' => 32,
                'created_at' => '2018-01-14 05:40:40',
                'updated_at' => '2018-01-14 05:40:40'
            ),
            
            array (
                'id' => 132,
                'name' => 'Dirección de Patrimonio Artistíco',
                'sre_cat_department_id' => 32,
                'created_at' => '2018-01-14 05:40:40',
                'updated_at' => '2018-01-14 05:40:40'
            ),
            
            array (
                'id' => 133,
                'name' => 'Dirección de Servicios Generales',
                'sre_cat_department_id' => 32,
                'created_at' => '2018-01-14 05:40:40',
                'updated_at' => '2018-01-14 05:40:40'
            ),
            
            array (
                'id' => 134,
                'name' => 'Promoción Cultural',
                'sre_cat_department_id' => 33,
                'created_at' => '2018-01-14 05:40:40',
                'updated_at' => '2018-01-14 05:40:40'
            ),
            
            array (
                'id' => 135,
                'name' => 'Proyectos Especiales',
                'sre_cat_department_id' => 33,
                'created_at' => '2018-01-14 05:40:40',
                'updated_at' => '2018-01-14 05:40:40'
            ),
            
            array (
                'id' => 136,
                'name' => 'Promoción Turística',
                'sre_cat_department_id' => 33,
                'created_at' => '2018-01-14 05:40:40',
                'updated_at' => '2018-01-14 05:40:40'
            ),
            
            array (
                'id' => 137,
                'name' => 'Dirección General Adjunta de Promoción Comercial y de Inversiones',
                'sre_cat_department_id' => 34,
                'created_at' => '2018-01-14 05:40:40',
                'updated_at' => '2018-01-14 05:40:40'
            ),
            
            array (
                'id' => 138,
                'name' => 'Dirección de Promoción Económica para Europa y América Latina',
                'sre_cat_department_id' => 34,
                'created_at' => '2018-01-14 05:40:40',
                'updated_at' => '2018-01-14 05:40:40'
            ),
            
            array (
                'id' => 139,
                'name' => 'Dirección de Promoción Económica para América del Norte, Asia-Pacífico, África y Medio Oriente',
                'sre_cat_department_id' => 34,
                'created_at' => '2018-01-14 05:40:40',
                'updated_at' => '2018-01-14 05:40:40'
            ),
            
            array (
                'id' => 140,
                'name' => 'Dirección General Adjunta de Análisis Económico',
                'sre_cat_department_id' => 34,
                'created_at' => '2018-01-14 05:40:40',
                'updated_at' => '2018-01-14 05:40:40'
            ),
            
            array (
                'id' => 141,
                'name' => 'Direccíón de Información y Análisis Económicos',
                'sre_cat_department_id' => 34,
                'created_at' => '2018-01-14 05:40:40',
                'updated_at' => '2018-01-14 05:40:40'
            ),
            
            array (
                'id' => 142,
                'name' => 'Dirección General Adjunta de Relaciones Económica con América Latina y el Caribe y Asia-Pacífico',
                'sre_cat_department_id' => 35,
                'created_at' => '2018-01-14 05:40:40',
                'updated_at' => '2018-01-14 05:40:40'
            ),
            
            array (
                'id' => 143,
                'name' => 'Dirección de Relaciones Económicas con América Latina y África',
                'sre_cat_department_id' => 35,
                'created_at' => '2018-01-14 05:40:40',
                'updated_at' => '2018-01-14 05:40:40'
            ),
            
            array (
                'id' => 144,
                'name' => 'Dirección de Relaciones Económicas con Europa',
                'sre_cat_department_id' => 35,
                'created_at' => '2018-01-14 05:40:40',
                'updated_at' => '2018-01-14 05:40:40'
            ),
            
            array (
                'id' => 145,
                'name' => 'Dirección General Adjunta de Cooperación Económica y Organismos Internacionales',
                'sre_cat_department_id' => 35,
                'created_at' => '2018-01-14 05:40:40',
                'updated_at' => '2018-01-14 05:40:40'
            ),
            
            array (
                'id' => 146,
                'name' => 'Dirección de Cooperación y Desarrollo Económico A',
                'sre_cat_department_id' => 35,
                'created_at' => '2018-01-14 05:40:40',
                'updated_at' => '2018-01-14 05:40:40'
            ),
            
            array (
                'id' => 147,
                'name' => 'Dirección de Cooperación y Desarrollo Económico B',
                'sre_cat_department_id' => 35,
                'created_at' => '2018-01-14 05:40:40',
                'updated_at' => '2018-01-14 05:40:40'
            ),
            
            array (
                'id' => 148,
                'name' => 'Dirección de Relaciones Económicas con Asia-Pacífico y Medio Oriente',
                'sre_cat_department_id' => 35,
                'created_at' => '2018-01-14 05:40:40',
                'updated_at' => '2018-01-14 05:40:40'
            ),
            
            array (
                'id' => 149,
                'name' => 'Dirección General Adjunta para Europa, Asia y América del Norte',
                'sre_cat_department_id' => 36,
                'created_at' => '2018-01-14 05:40:40',
                'updated_at' => '2018-01-14 05:40:40'
            ),
            
            array (
                'id' => 150,
                'name' => 'Dirección de Cooperación Sur-Sur y Alianza del Pacífico',
                'sre_cat_department_id' => 36,
                'created_at' => '2018-01-14 05:40:40',
                'updated_at' => '2018-01-14 05:40:40'
            ),
            
            array (
                'id' => 151,
                'name' => 'Dirección de Ayuda Humanitaria',
                'sre_cat_department_id' => 36,
                'created_at' => '2018-01-14 05:40:40',
                'updated_at' => '2018-01-14 05:40:40'
            ),
            
            array (
                'id' => 152,
                'name' => 'Dirección General Adjunta para la Cooperación Académica',
                'sre_cat_department_id' => 36,
                'created_at' => '2018-01-14 05:40:40',
                'updated_at' => '2018-01-14 05:40:40'
            ),
            
            array (
                'id' => 153,
                'name' => 'Dirección General Adjunta para Desarrollo Económico e Infraestructura',
                'sre_cat_department_id' => 37,
                'created_at' => '2018-01-14 05:40:40',
                'updated_at' => '2018-01-14 05:40:40'
            ),
            
            array (
                'id' => 154,
                'name' => 'Dirección General Adjunta de Desarrollo Social-Humano y Sostenible',
                'sre_cat_department_id' => 37,
                'created_at' => '2018-01-14 05:40:40',
                'updated_at' => '2018-01-14 05:40:40'
            ),
            
            array (
                'id' => 155,
                'name' => 'Dirección General Adjunta de Vinculación y Cooperación Bilateral con Centroamérica y el Caribe',
                'sre_cat_department_id' => 37,
                'created_at' => '2018-01-14 05:40:40',
                'updated_at' => '2018-01-14 05:40:40'
            ),
            
            array (
                'id' => 156,
                'name' => 'Dirección General Adjunta de Gestión Administrativa',
                'sre_cat_department_id' => 38,
                'created_at' => '2018-01-14 05:40:40',
                'updated_at' => '2018-01-14 05:40:40'
            ),
            
            array (
                'id' => 157,
                'name' => 'Dirección de Archivos',
                'sre_cat_department_id' => 38,
                'created_at' => '2018-01-14 05:40:40',
                'updated_at' => '2018-01-14 05:40:40'
            ),
            
            array (
                'id' => 158,
                'name' => 'Dirección de Documentación y Difusión',
                'sre_cat_department_id' => 38,
                'created_at' => '2018-01-14 05:40:41',
                'updated_at' => '2018-01-14 05:40:41'
            ),
            
            array (
                'id' => 159,
                'name' => 'Dirección de Historia Diplomática y Publicaciones',
                'sre_cat_department_id' => 38,
                'created_at' => '2018-01-14 05:40:41',
                'updated_at' => '2018-01-14 05:40:41'
            ),
        ));
        
        
    }
}