<?php

use Illuminate\Database\Seeder;

class SreCatOfficeTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        
        \DB::table('sre_cat_office_types')->insert(array (
            
            array (
                'id' => 1,
                'name' => 'Delegación Metropolitana',
                'created_at' => '2018-01-14 05:40:25',
                'updated_at' => '2018-01-14 05:40:25'
            ),
            
            array (
                'id' => 2,
                'name' => 'Delegación Foránea',
                'created_at' => '2018-01-14 05:40:25',
                'updated_at' => '2018-01-14 05:40:25'
            ),
            
            array (
                'id' => 3,
                'name' => 'Oficina de Enlace en la Zona Metropolitana de la Ciudad de México',
                'created_at' => '2018-01-14 05:40:25',
                'updated_at' => '2018-01-14 05:40:25'
            ),
            
            array (
                'id' => 4,
                'name' => 'Oficina Central',
                'created_at' => '2018-01-14 05:40:25',
                'updated_at' => '2018-01-14 05:40:25'
            ),
            
            array (
                'id' => 5,
                'name' => 'Embajada',
                'created_at' => '2018-01-14 05:40:25',
                'updated_at' => '2018-01-14 05:40:25'
            ),
            
            array (
                'id' => 6,
                'name' => 'Consulado',
                'created_at' => '2018-01-14 05:40:25',
                'updated_at' => '2018-01-14 05:40:25'
            ),
            
            array (
                'id' => 7,
                'name' => 'Sección Cunsular',
                'created_at' => '2018-01-14 05:40:25',
                'updated_at' => '2018-01-14 05:40:25'
            ),
            
            array (
                'id' => 8,
                'name' => 'Misión',
                'created_at' => '2018-01-14 05:40:25',
                'updated_at' => '2018-01-14 05:40:25'
            ),
            
            array (
                'id' => 9,
                'name' => 'Oficina de Enlace en el Exterior',
                'created_at' => '2018-01-14 05:40:25',
                'updated_at' => '2018-01-14 05:40:25'
            ),
        ));
        
        
    }
}