<?php

use Illuminate\Database\Seeder;

class SreCatProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sre_cat_profiles')->insert(
	    	[
	    	    [ 'name' => 'Administrador' ],
                [ 'name' => 'Autorizador' ],
                [ 'name' => 'Consulta' ],
                [ 'name' => 'Operador' ],
                [ 'name' => 'Revisor' ]
		    ]
	    );
    }
}
