<?php

use Illuminate\Database\Seeder;

class SreCatAgreementTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('sre_cat_agreement_types')->insert(
	    	[
			    [ 'name' => 'Determinado' ],
			    [ 'name' => 'Indeterminado' ]
		    ]
	    );
    }
}
