<?php

use Illuminate\Database\Seeder;

class SreCatDocumentTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('sre_cat_document_types')->insert(
	    	[
			    ['name' => 'Identificación Oficial'],
			    ['name' => 'Documentación Migratoria'],
				['name' => 'Acta de Nacimiento'],
			    ['name' => 'Pasaporte'],
				['name'	=> 'Visa'],
				['name' => 'Comprobante de Domicilio'],
			    ['name' => 'Acta de Matrimonio'],
			    ['name' => 'Comprobante de Estudios'],
			    ['name' => 'Cédula Profesional'],
			    ['name' => 'Carta de Recomendación']
		    ]
	    );
    }
}
