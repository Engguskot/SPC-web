<?php

use Illuminate\Database\Seeder;

class GeoCatRegionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        
        \DB::table('geo_cat_regions')->insert(array (
            
            array (
                'id' => 1,
                'name' => 'África Oriental',
                'geo_cat_continent_id' => 1,
                'created_at' => '2018-01-14 04:03:09',
                'updated_at' => '2018-01-14 04:03:09',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 2,
                'name' => 'África Central',
                'geo_cat_continent_id' => 1,
                'created_at' => '2018-01-14 04:03:09',
                'updated_at' => '2018-01-14 04:03:09',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 3,
                'name' => 'África del Norte',
                'geo_cat_continent_id' => 1,
                'created_at' => '2018-01-14 04:03:09',
                'updated_at' => '2018-01-14 04:03:09',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 4,
                'name' => 'África Austral',
                'geo_cat_continent_id' => 1,
                'created_at' => '2018-01-14 04:03:09',
                'updated_at' => '2018-01-14 04:03:09',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 5,
                'name' => 'África Occidental',
                'geo_cat_continent_id' => 1,
                'created_at' => '2018-01-14 04:03:09',
                'updated_at' => '2018-01-14 04:03:09',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 6,
                'name' => 'América del Norte',
                'geo_cat_continent_id' => 2,
                'created_at' => '2018-01-14 04:03:09',
                'updated_at' => '2018-01-14 04:03:09',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 7,
                'name' => 'América Latina y el Caribe',
                'geo_cat_continent_id' => 2,
                'created_at' => '2018-01-14 04:03:09',
                'updated_at' => '2018-01-14 04:03:09',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 8,
                'name' => 'Asia Central',
                'geo_cat_continent_id' => 4,
                'created_at' => '2018-01-14 04:03:09',
                'updated_at' => '2018-01-14 04:03:09',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 9,
                'name' => 'Asia Oriental',
                'geo_cat_continent_id' => 4,
                'created_at' => '2018-01-14 04:03:10',
                'updated_at' => '2018-01-14 04:03:10',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 10,
                'name' => 'Asia del Sur',
                'geo_cat_continent_id' => 4,
                'created_at' => '2018-01-14 04:03:10',
                'updated_at' => '2018-01-14 04:03:10',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 11,
                'name' => 'Sudeste Asiático',
                'geo_cat_continent_id' => 4,
                'created_at' => '2018-01-14 04:03:10',
                'updated_at' => '2018-01-14 04:03:10',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 12,
                'name' => 'Asia Occidental',
                'geo_cat_continent_id' => 4,
                'created_at' => '2018-01-14 04:03:10',
                'updated_at' => '2018-01-14 04:03:10',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 13,
                'name' => 'Europa Oriental',
                'geo_cat_continent_id' => 5,
                'created_at' => '2018-01-14 04:03:10',
                'updated_at' => '2018-01-14 04:03:10',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 14,
                'name' => 'Europa del Norte',
                'geo_cat_continent_id' => 5,
                'created_at' => '2018-01-14 04:03:10',
                'updated_at' => '2018-01-14 04:03:10',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 15,
                'name' => 'Europa del Sur',
                'geo_cat_continent_id' => 5,
                'created_at' => '2018-01-14 04:03:10',
                'updated_at' => '2018-01-14 04:03:10',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 16,
                'name' => 'Europa Occidental',
                'geo_cat_continent_id' => 5,
                'created_at' => '2018-01-14 04:03:10',
                'updated_at' => '2018-01-14 04:03:10',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 17,
                'name' => 'Australia y Nueva Zelanda',
                'geo_cat_continent_id' => 6,
                'created_at' => '2018-01-14 04:03:10',
                'updated_at' => '2018-01-14 04:03:10',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 18,
                'name' => 'Melanesia',
                'geo_cat_continent_id' => 6,
                'created_at' => '2018-01-14 04:03:10',
                'updated_at' => '2018-01-14 04:03:10',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 19,
                'name' => 'Micronesia',
                'geo_cat_continent_id' => 6,
                'created_at' => '2018-01-14 04:03:10',
                'updated_at' => '2018-01-14 04:03:10',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 20,
                'name' => 'Polinesia',
                'geo_cat_continent_id' => 6,
                'created_at' => '2018-01-14 04:03:10',
                'updated_at' => '2018-01-14 04:03:10',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}