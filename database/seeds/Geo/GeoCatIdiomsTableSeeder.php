<?php

use Illuminate\Database\Seeder;

class GeoCatIdiomsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        
        \DB::table('geo_cat_idioms')->insert(array (
            
            array (
                'id' => 1,
                'name' => 'Afgano',
                'created_at' => '2018-01-14 04:03:07',
                'updated_at' => '2018-01-14 04:03:07',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 2,
                'name' => 'Alemán',
                'created_at' => '2018-01-14 04:03:07',
                'updated_at' => '2018-01-14 04:03:07',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 3,
                'name' => 'Amárico',
                'created_at' => '2018-01-14 04:03:07',
                'updated_at' => '2018-01-14 04:03:07',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 4,
                'name' => 'Árabe',
                'created_at' => '2018-01-14 04:03:07',
                'updated_at' => '2018-01-14 04:03:07',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 5,
                'name' => 'Camboyano',
                'created_at' => '2018-01-14 04:03:07',
                'updated_at' => '2018-01-14 04:03:07',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 6,
                'name' => 'Chino',
                'created_at' => '2018-01-14 04:03:07',
                'updated_at' => '2018-01-14 04:03:07',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 7,
                'name' => 'Coreano',
                'created_at' => '2018-01-14 04:03:07',
                'updated_at' => '2018-01-14 04:03:07',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 8,
                'name' => 'Danés',
                'created_at' => '2018-01-14 04:03:07',
                'updated_at' => '2018-01-14 04:03:07',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 9,
                'name' => 'Español',
                'created_at' => '2018-01-14 04:03:07',
                'updated_at' => '2018-01-14 04:03:07',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 10,
                'name' => 'Estonio',
                'created_at' => '2018-01-14 04:03:07',
                'updated_at' => '2018-01-14 04:03:07',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 11,
                'name' => 'Finlandés',
                'created_at' => '2018-01-14 04:03:07',
                'updated_at' => '2018-01-14 04:03:07',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 12,
                'name' => 'Francés',
                'created_at' => '2018-01-14 04:03:07',
                'updated_at' => '2018-01-14 04:03:07',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 13,
                'name' => 'Griego',
                'created_at' => '2018-01-14 04:03:07',
                'updated_at' => '2018-01-14 04:03:07',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 14,
                'name' => 'Hebreo',
                'created_at' => '2018-01-14 04:03:07',
                'updated_at' => '2018-01-14 04:03:07',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 15,
                'name' => 'Holandés',
                'created_at' => '2018-01-14 04:03:08',
                'updated_at' => '2018-01-14 04:03:08',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 16,
                'name' => 'Indonesio',
                'created_at' => '2018-01-14 04:03:08',
                'updated_at' => '2018-01-14 04:03:08',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 17,
                'name' => 'Inglés',
                'created_at' => '2018-01-14 04:03:08',
                'updated_at' => '2018-01-14 04:03:08',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 18,
                'name' => 'Italiano',
                'created_at' => '2018-01-14 04:03:08',
                'updated_at' => '2018-01-14 04:03:08',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 19,
                'name' => 'Japonés',
                'created_at' => '2018-01-14 04:03:08',
                'updated_at' => '2018-01-14 04:03:08',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 20,
                'name' => 'Laosiano',
                'created_at' => '2018-01-14 04:03:08',
                'updated_at' => '2018-01-14 04:03:08',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 21,
                'name' => 'Letón',
                'created_at' => '2018-01-14 04:03:08',
                'updated_at' => '2018-01-14 04:03:08',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 22,
                'name' => 'Lituano',
                'created_at' => '2018-01-14 04:03:08',
                'updated_at' => '2018-01-14 04:03:08',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 23,
                'name' => 'Malayo',
                'created_at' => '2018-01-14 04:03:08',
                'updated_at' => '2018-01-14 04:03:08',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 24,
                'name' => 'Noruego',
                'created_at' => '2018-01-14 04:03:08',
                'updated_at' => '2018-01-14 04:03:08',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 25,
                'name' => 'Persa',
                'created_at' => '2018-01-14 04:03:08',
                'updated_at' => '2018-01-14 04:03:08',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 26,
                'name' => 'Polaco',
                'created_at' => '2018-01-14 04:03:08',
                'updated_at' => '2018-01-14 04:03:08',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 27,
                'name' => 'Portugués',
                'created_at' => '2018-01-14 04:03:08',
                'updated_at' => '2018-01-14 04:03:08',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 28,
                'name' => 'Rumano',
                'created_at' => '2018-01-14 04:03:08',
                'updated_at' => '2018-01-14 04:03:08',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 29,
                'name' => 'Ruso',
                'created_at' => '2018-01-14 04:03:08',
                'updated_at' => '2018-01-14 04:03:08',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 30,
                'name' => 'Sueco',
                'created_at' => '2018-01-14 04:03:08',
                'updated_at' => '2018-01-14 04:03:08',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 31,
                'name' => 'Suizo',
                'created_at' => '2018-01-14 04:03:08',
                'updated_at' => '2018-01-14 04:03:08',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 32,
                'name' => 'Tagalo',
                'created_at' => '2018-01-14 04:03:08',
                'updated_at' => '2018-01-14 04:03:08',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 33,
                'name' => 'Tailandés',
                'created_at' => '2018-01-14 04:03:08',
                'updated_at' => '2018-01-14 04:03:08',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 34,
                'name' => 'Turco',
                'created_at' => '2018-01-14 04:03:08',
                'updated_at' => '2018-01-14 04:03:08',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 35,
                'name' => 'Ucraniano',
                'created_at' => '2018-01-14 04:03:08',
                'updated_at' => '2018-01-14 04:03:08',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 36,
                'name' => 'Vietnamita',
                'created_at' => '2018-01-14 04:03:08',
                'updated_at' => '2018-01-14 04:03:08',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}