<?php

use Illuminate\Database\Seeder;

class AdminFormFieldTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('admin_forms_field_types')->insert(
	    	[
			    [ 'name' => 'Respuesta corta' ],
			    [ 'name' => 'Párrafo' ],
                [ 'name' => 'Selección múltiple' ],
                [ 'name' => 'Casilla de verificación' ],
                [ 'name' => 'Desplegable' ],
                [ 'name' => 'Fecha' ],
                [ 'name' => 'Hora' ],
                [ 'name' => 'Archivo' ],
		    ]
	    );
    }
}
