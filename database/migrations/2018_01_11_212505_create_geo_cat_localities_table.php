<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeoCatLocalitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geo_cat_localities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->char('postalCode', 10)->nullable();
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->integer('geo_cat_municipality_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('geo_cat_municipality_id')
                  ->references('id')
                  ->on('geo_cat_municipalities');

            $table->index([
                        'name',
                        'postalCode',
                        'latitude',
                        'longitude'
                    ],'geo_cat_localities_column_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
