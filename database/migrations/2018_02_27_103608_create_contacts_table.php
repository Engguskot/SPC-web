<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('geo_cat_country_id')->unsigned()->nullable();
            $table->integer('geo_cat_state_id')->unsigned()->nullable();
            $table->integer('geo_cat_municipality_id')->unsigned()->nullable();
            $table->string('phone',500)->nullable();
            $table->string('mail',500)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('geo_cat_country_id')->references('id')->on('geo_cat_countries');
            $table->foreign('geo_cat_state_id')->references('id')->on('geo_cat_states');
            $table->foreign('geo_cat_municipality_id')->references('id')->on('geo_cat_municipalities');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
