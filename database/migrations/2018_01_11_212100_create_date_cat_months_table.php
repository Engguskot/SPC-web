<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDateCatMonthsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('date_cat_months', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('shortName');
            $table->timestamps();
            $table->softDeletes();

            $table->index([
                        'name',
                    ]);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
