<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSreCatSubAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sre_cat_sub_areas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('sre_cat_area_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('sre_cat_area_id')
                  ->references('id')
                  ->on('sre_cat_areas');

            $table->index([
                        'name'
                    ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
