<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeoCatCurrencyExchangeRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geo_cat_currency_exchange_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->char('year', 4)->nullable();
            $table->integer('date_cat_month_id')->unsigned()->nullable();
            $table->integer('geo_cat_currency_id')->unsigned();
            $table->date('date')->nullable();
            $table->decimal('value')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('geo_cat_currency_id')
                  ->references('id')
                  ->on('geo_cat_currencies');

            $table->foreign('date_cat_month_id')
                  ->references('id')
                  ->on('date_cat_months');


            $table->index([
                        'year',
                        'date',
                        'value'
                    ]);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
