<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            // Users
            Schema::dropIfExists('sre_job_employee_histories');
            Schema::dropIfExists('sre_users');
            // Employees
            Schema::dropIfExists('sre_dependant_documents');
            Schema::dropIfExists('sre_dependants');
            Schema::dropIfExists('sre_employee_documents');
            Schema::dropIfExists('sre_cat_document_types');
            Schema::dropIfExists('sre_employee_idioms');
            Schema::dropIfExists('sre_employee_studies');
            Schema::dropIfExists('sre_employees');
            // Jobs
            Schema::dropIfExists('sre_jobs');
            // Catalogs
            Schema::dropIfExists('sre_cat_job_statuses');
            Schema::dropIfExists('sre_cat_employee_statuses');
            Schema::dropIfExists('sre_cat_employee_types');
            Schema::dropIfExists('sre_cat_employee_separation_types');
            Schema::dropIfExists('sre_cat_education_levels');
            Schema::dropIfExists('sre_cat_employee_marital_statuses');
            Schema::dropIfExists('sre_cat_employee_genders');
            Schema::dropIfExists('sre_cat_dependant_types');
            Schema::dropIfExists('sre_cat_sub_departments');
            Schema::dropIfExists('sre_cat_departments');
            Schema::dropIfExists('sre_cat_sub_areas');
            Schema::dropIfExists('sre_cat_areas');
            Schema::dropIfExists('sre_cat_offices');
            Schema::dropIfExists('sre_cat_office_types');
            Schema::dropIfExists('sre_cat_profiles');
            // Geo
            Schema::dropIfExists('geo_cat_currency_exchange_rates');
            Schema::dropIfExists('geo_cat_localities');
            Schema::dropIfExists('geo_cat_municipalities');
            Schema::dropIfExists('geo_cat_states');
            Schema::dropIfExists('geo_cat_countries');;
            Schema::dropIfExists('geo_cat_regions');
            Schema::dropIfExists('geo_cat_continents');
            Schema::dropIfExists('geo_cat_currencies');
            Schema::dropIfExists('geo_cat_idioms');
            Schema::dropIfExists('date_cat_months');
            // Files
            Schema::dropIfExists('sre_files');
        });
    }
}
